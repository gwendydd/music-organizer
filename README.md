# Projet: music-organizer

Auteurs: Michael Kölling and David J. Barnes
Traduction: Laurent Pierron


Ce projet fait partie du matériel pour le livre

   **Objects First with Java - A Practical Introduction using BlueJ
   Fifth edition
   David J. Barnes and Michael Kölling
   Pearson Education, 2012**

Il est expliqué dans le chapitre 4.

Une réalisation d'un gestionnaire pour des fichiers musicaux ; utilisé pour introduire
les collections et les boucles. Ce projet comprend la possibilité de jouer des fichiers MP3.
Une version graphique sera ajoutée plus tard.

Le projet va être développé de manière incrémentale,
c'est à dire en ajoutant des fonctionnalités au fil du développement.
C'est une approche nécessaire pour développer un logiciel conséquent.


- - -

## TÂCHE 1 : Préparation du projet

1. Créer une bifurcation (fork) du projet **music-organizer** dans votre compte Bitbucket.
2. Ajouter votre binôme et `lcpierron`en administrateur du projet.
3. Cloner votre projet sur votre machine locale.
4. Créer une branche locale de la version 1 du projet :

    `git checkout -b monboulot v1`

- - -

## Classe `MusicOrganizer`

À ce niveau, le projet contient une seule classe `MusicOrganizer`, la dernière version contiendra 5 classes et la version graphique 2 classes supplémentaires.

Le but du projet étant de gérer une **collection** de fichiers musicaux, nous allons utiliser
une bibliothèque Java qui permet de conserver et manipuler une collection de données.
Pour ce projet nous choisissons la classe `ArrayList`, vous pouvez consulter la documentation de la classe ici :
 [http://docs.oracle.com/javase/8/docs/api/java/util/ArrayList.html](http://docs.oracle.com/javase/8/docs/api/java/util/ArrayList.html)

Voici le début de la déclaration de la classe avec la déclaration de l'attribut `files` qui stockera la liste
des fichiers musicaux :

```java
public class MusicOrganizer
{
  // An ArrayList for storing the file names of music files.
  private ArrayList<String> files;
  ...
}
```

Cette première version du logiciel, doit permettre :

  - d'ajouter des noms de fichiers musicaux à la liste, il n'y a pas de limites ;
  - de calculer le nombre de pistes audios dans la collection ;
  - de lister toutes les pistes ;

### TÂCHE 2 : Test de création d'objets `MusicOrganizer`

Pour explorer la classe `MusicOrganizer` vous allez créer des tests. Le premier consistera à valider la création de la classe avec une liste de longueur nulle :

1. Créer la classe de test associée à la `MusicOrganizer`.

1. Créez un premier test qui vérifiera que la taille de la liste est 0, quand l'objet vient d'être créé, choisir un nom long explicite.

1. Quand votre test fonctionne sauvegardez votre travail : `git add MusicOrganizerTest.java MusicOrganizerTest.ctxt` puis `git commit -a -m "Test que la liste créée vaut 0."`.

### TÂCHE 3 : Modification de la méthode `listFile`

Afin de pouvoir tester plus facilement le comportement des objets `MusicOrganizer`, vous allez modifier la méthode `listFile` afin qu'elle retourne le nom du fichier musical en plus de l'affichage dans la console.

La nouvelle signature de la méthode sera : `public String listFile(int index)`

1. Modifiez la méthode pour retourner `filename` ou la chaîne vide `""`

1. Modifiez la Javadoc pour ajouter l'information `@return`, voir la méthode `getNumberOfFiles` en exemple.

1. Ajoutez deux ou plusieurs tests dans la classe `MusicOrganizerTest` pour contrôler que votre méthode renvoie le nom du fichier souhaité ou la chaîne vide.

1. Enregistrez votre travail sous Git avec un commentaire approprié.

### TÂCHE 4 : Création d'un objet initial pour tous les tests

Pour réaliser les tests suivants vous allez créer un objet `MusicOrganizer`, qui contiendra déjà trois pistes musicales.

1. Videz le banc des objets en utilisant le menu `Outils -> Réinitialiser la machine`.

1. Avec le menu contextuel créez un objet `MusicOrganizer` que vous nommerez `musicFileOfThree`, l'objet apparaît dans la liste en bas à gauche.

2. Ajoutez trois noms de fichiers musicaux à cet objet.

3. En double-cliquant sur l'objet, explorez le contenu de l'objet.

4. Dans le menu contextuel de `MusicOrganizerTest` choisissez `Bureau Objets --> Engagements`, cela copiera votre objet `musicFileOfThree`, comme variable d'instance dans la classe `MusicOrganizerTest`.

5. Ouvrez dans l'éditeur `MusicOrganizerTest` et vous verrez votre objet initialisé dans la méthode `setUp()`, cette méthode sera appelée avant chaque test.

### TÂCHE 5 : Test élaboré de la méthode `removeFile`

Maintenant vous allez utiliser l'objet `musicFileOfThree` pour tester la méthode `removeFile`. Vous allez également ajouter plusieurs assertions dans la méthode de test, afin de vérifier le comportement complet de la méthode.

1. Démarrez l'enregistrement d'un nouveau test, nommez-le `removeFirstFileOfList` ; l'objet `musicFileOfThree` apparaît sur le banc des objets.

1. Appelez la méthode `getNumberOfFiles()` sur cet objet, elle doit retourner 3, ajoutez l'assertion.

1. Appelez la méthode `listFile(0)` sur cet objet, elle doit retourner le nom de votre premier fichier, ajoutez l'assertion.

1. Appelez la méthode `removeFile(0)` sur votre objet, c'est ici que l'on effectue l'opération que l'on veut tester, avant vous avez testé les préconditions à votre test.

1. 1. Appelez la méthode `getNumberOfFiles()` sur cet objet, elle doit retourner 2, ajoutez l'assertion.

1. Appelez la méthode `listFile(0)` sur cet objet, elle doit retourner le nom de votre second fichier, ajoutez l'assertion.

1. Cliquez sur le bouton `Terminer`.

1. Compilez et exécutez les tests, assurez-vous qu'ils passent tous sinon vous pouvez corriger votre test dans l'éditeur.

1. Enregistrez votre nouvelle version sous Git avec le commentaire adapté.

### TÂCHE 6 : Tests complémentaires de la méthode `removeFile`

Vous avez testé la suppression du premier élément de la liste, mais il faut aussi tester la suppression du dernier élément de la liste et d'un élément du milieu.

Quand on définit des tests, on identifie les cas particuliers à tester et les cas généraux. Les cas particuliers dépendent des objets à tester, sur une liste de classe `ArrayList` ce sera le premier et le dernier élément ainsi que la liste vide, sur des nombres entiers ce sera 0, 1, -1, des nombres pairs, des nombres impairs, des nombres positifs et négatifs très grands (2^64 par exemple).

Vous allez réaliser un test de suppression du dernier fichier :

1. Ouvrez la classe `MusicOrganizerTest` dans l'éditeur.

1. Dupliquez le test `removeFirstFileOfList` et nommez le `removeLastFileOfList`

1. Modifiez le corps de ce test pour qu'il teste la suppression du dernier fichier.

1. Quand vous avez réussi à compiler et exécuter tous les tests, ajoutez une nouvelle méthode pour tester la suppression du fichier du milieu.

1. Une fois que tous les tests passent, enregistrez sous Git votre travail.

1. Synchronisez votre travail sur le serveur : `git push --all`

- - -

## Amélioration de la classe `MusicOrganizer`

### TÂCHE 7 : Ajout de la méthode `validIndex`

1. Créer une méthode `validIndex`, qui prend un entier en paramètre et retourne un booléen dont la valeur est `true` si l'entier est un index de fichier valide pour la collection courante et `false` sinon.

1. Écrire la Javadoc correspondante à cette méthode.

1. Écrire au moins deux tests pour valider votre nouvelle méthode.

1. Quand tous les tests passent, enregistrez sous Git votre nouvelle version.


### TÂCHE 8 : Modification des méthodes `listFile` et `removeFile`

Maintenant vous allez utiliser la méthode créée précédemment pour simplifier les autres méthodes.

1. Simplifiez `listFile` avec la méthode `validIndex`.

1. Simplifiez `removeFile` avec la méthode `validIndex`.

1. Exécutez tous les tests. Vous n'avez pas de nouveaux tests à écrire car le comportement n'a pas changé, et si ils passent tous vous êtes sûrs qu'il n'y a pas eu de *régression* dans votre programme.

1. Enregistrez votre nouvelle version sous Git.

1. Synchronisez cette version sur le serveur : `git push --all`

- - -

## Jouons un peu de musique

Nous allons maintenant compléter notre programme pour jouer les pistes musicales, pour cela nous allons utiliser la version `v2` de `music-player`

1. Quittez BlueJ

1. Créez une nouvelle branche à partir de 'v2' : `git checkout -b mon-tp-v2 v2`

1. Ajoutez depuis la branche 'master' la bibliothèque qui permet de jouer les fichiers MP3 : `git checkout master +libs`. La commande `checkout` permet de de déplacer dans des branches, mais également de copier des fichiers qui sont dans d'autres branches vers la branche courante.

1. Ouvrez le projet music-organizer dans BlueJ.

Dans cette nouvelle version, vous avez une nouvelle classe `MusicPlayer`, qui dispose de trois méthodes pour jouer de la musique.

Pour tester l'objet `MusicPlayer` vous avez des exemples de fichiers audio MP3 dans le répertoire audio du projet.

1. Créez la classe de test associée à la classe `MusicPlayer`.

1. Ajoutez un test pour tester la méthode `playSample`.

1. Ajoutez un test pour tester la méthode `startPlaying`, ajoutez également l'appel à la méthode `stop`, que se passe-t-il si il n'y a pas de méthode `stop`.

   > Pour arrêter de jouer la musique, vous pouvez utiliser le menu `Outils -> Réinitialiser la machine` ou alors quitter BlueJ.

 1. Ajoutez un test pour tester la méthode `startPlaying` de la classe `MusicOragnizer`

 1. Enregistrez sous Git vos modifications.

 1. Synchronisez sur le serveur : `git push --all`


- - -


## Seconde partie du projet

### TÂCHE 9 : Ajout d'une méthode `listaAllFiles`

Dans la classe `MusicOrganizer`, vous devez ajouter une méthode
de signature `public void listAllFiles()`, qui doit afficher
dans la `console` la liste des pistes.

Cette méthode devra utiliser l'instruction d'itération :

```java
for (Type objet : collection) {
  // Instructions utilisant objet
}
```

Cette instruction permet de parcourir tous les éléments d'une collection.

1. Créez la méthode `listAllFiles`
2. Testez le comportement de cette méthode en ajoutant plusieurs fichiers.
3. N'oubliez pas d'enregistrer votre travail avec la commande `git commit`
4. Modifiez `listAllFiles` pour ajouter la position du fichier dans la liste,
exemple de sortie :

```
01 : Le printemps
02 : L'été
03 : L'automne
04 : L'hiver
...
```

5. Testez et *committez*.


### TÂCHE 10 : Ajout d'une méthode `listMatching`

Dans la classe `MusicOrganizer`, vous devez ajouter une méthode
de signature `public void listMatching(String searchString)`, qui doit afficher
dans la `console` toutes les pistes qui ont le mot passé en
paramètre dans leur titre.


1. Créez la méthode `listMatching`
2. Testez le comportement de cette méthode en ajoutant plusieurs fichiers.
3. N'oubliez pas d'enregistrer votre travail avec la commande `git commit`.
4. Modifiez `listMatching` pour ajouter la position du fichier dans la liste,
exemple de sortie :

```
01 : Le printemps
02 : L'été
03 : L'automne
04 : L'hiver
...
```

5. Testez et *committez*.
6. Ajoutez un message si aucun titre ne correspond au texte recherché.
7. Modifiez la méthode pour retourner la liste des titres sélectionnés
7. Créez une méthode `playTracksSamples`, qui joue un extrait de chacun des titres
passés en paramètre.
8. Ecrivez une méthode `playMatchingSamples(String searchString)`, qui joue
un extrait des chansons trouvées par la `searchString`, un petit `jingle` par
défaut sera joué si aucun titre n'est trouvé (à vous de chercher un jingle sur
  Internet)
9. Testez et *committez*
10. Synchronisez sur le serveur `BitBucket`: `git push --all`

### TÂCHE 11 : Utilisation d'objets `Track` pour les pistes

Nous allons maintenant compléter le programme finale pour faire un peu plus
de gestion de pistes. Pour cela nous allons utiliser la version `v5` de `music-player`

1. Quittez BlueJ

1. Créez une nouvelle branche :

  `git checkout -b mon-tp-v5 v5`

1. Ouvrez le projet `music-organizer` dans BlueJ.

Dans cette nouvelle version, il y a deux nouvelles classes :

    - `TrackReader` : permet de lire tous les fichiers dans un dossier
    - `Track` : classe utilisée pour représenter un morceau de musique.

Une première amélioration de la classe `MusicOrganizer` :

1. Ajoutez un constructeur à la classe `MusicOrganizer` pour pouvoir passer
le dossier des fichiers musicaux en paramètre.
1. Modifiez le constructeur initial pour prendre en compte ce nouveau constructeur
et éviter la duplication de code
1. Testez et *committez*


Votre second travail, consiste à ajouter un compteur du nombre de fois
où un morceau a été joué.

1. Dans la classe `Track` ajoutez un attribut `playCount`
1. Ajoutez les méthodes nécessaires à l'utilisation de cet attribut (accesseur et mutateur)
1. Modifiez la méthode `getDetails`
1. Testez et `committez`
1. Dans `MusicOrganizer` ajoutez l'incrément du compteur à chaque fois qu'un morceau est joué

Quand un second morceau de musique est joué pendant qu'un premier est en cours,
les deux sont joués simultanément.

1. Modifiez le programme pour stopper le premier morceau quand vous en jouez un nouveau

La plupart des lecteurs de musique ont une fonction pour jouer les morceaux
de musique dans un ordre aléatoire, votre lecteur doit avoir cette fonction :

1. ajoutez une méthode `playTrack` à la classe `MusicPlayer` pour jouer un fichier musical de manière synchrone comme dans `playSample`et contrairement à `startPlaying` qui fonctionne de manière asynchrone.
1; ajoutez une méthode `randomPlay`qui permet de jouer de manière aléatoire la liste
des pistes
1. ajoutez une méthode de même nom qui prend une liste de pistes à jouer en paramètres,
si cette liste  est vide on prendra la liste complète des titres
1. Testez et `committez`

Pour faire une petite pause entre les morceaux de musique, vous pourrez avoir besoin d'une méthode qui interrompt l'exécution du programme principal. Cette méthode s'appelle `sleep(int ms)` elle se trouve dans la classe `Thread` du package `java.lang` (http://docs.oracle.com/javase/8/docs/api/java/lang/Thread.html). Pour l'utiliser ajoutez la méthode `wait(int milliseconds)` ci-dessous à votre classe et insérez `wait(2000);` par exemple dans votre code à l'endroit où vous souhaitez une pause,  le temps de pause est donné en millisecondes :

```java
    /**
     * Waits for a specified number of milliseconds before finishing.
     * This provides an easy way to specify a small delay which can be
     * used when producing animations.
     * @param  milliseconds  the number 
     */
    public void wait(int milliseconds)
    {
        try
        {
            Thread.sleep(milliseconds);
        } 
        catch (InterruptedException e)
        {
            // ignoring exception at the moment
        }
    }
```

### TÂCHE 12 Suppression de pistes

La majorité des logiciels de musique permettent de supprimer des morceaux, vous
allez donc ajouter cette fonctionnalité.

Pour faire cela vous allez un objet spécial associé aux collection
appelé `Iterator` (http://docs.oracle.com/javase/8/docs/api/java/util/Iterator.html).
Ne manquez pas de lire cette documentation.

La classe `Iterator<E>` est en fait une classe **interface**, qui ne permet pas
de créer des objets mais qui définit certaines méthodes que devront implémenter
les classes **concrètes** l'utilisant. Nous y reviendrons plus tard.

Le corps de votre méthode devra contenir un équivalent de cette suite
d'instructions, qui permet de supprimer les objets d'une collection :

```java
Iterator<Track> it = tracks.iterator();
while (it.hasNext()) {
  Track t = it.next();
  String artist = t.getArtist();
  if (artist.equals(artistToRemove)) {
    it.remove();
  }
}
```

1. Ecrire la méthode `removeArtistTrack`, qui supprimera les morceaux d'un artiste
dont le nom est passé en paramètre.
1. Testez et `committez`
1. Synchronisez votre travail sur *Bitbucket* : `git push --all`